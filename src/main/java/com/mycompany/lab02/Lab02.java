/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab02;

/**
 *
 * @author user
 */
import java.util.*;
public class Lab02 {
    static char[][] table = {{'-', '-', '-'} ,{'-', '-', '-'} ,{'-', '-', '-'}};
    static char CurrentPlayer = 'X';
    static int row,col ;
    static int count = 0;
    
    static void printWelcome(){
        System.out.println("Welcome To XOXO GAME!!");
    }
    static void printTable(){
        for(int i=0;i<3;i++){
            System.out.println("-------------");
            System.out.print(" |");
            for(int j=0;j<3;j++){
                System.out.print(table [i][j]+ " |");
            }
            System.out.println("");
        }
        System.out.println("-------------");
    }
    static void printTurn(){
        System.out.println(CurrentPlayer + " Turn");
    }
    static void inputRowCol(){
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input row,col :");
        row = sc.nextInt();
        col = sc.nextInt();
        if(table[row-1][col-1]!='-'){
            continue;
        }
        table[row-1][col-1] = CurrentPlayer;
        break;
        }
        count += 1;
    }
    static void switchPlayer(){
        if(CurrentPlayer=='X'){
            CurrentPlayer='O';
        }else{
            CurrentPlayer='X';
        } 
    }

    static boolean checkRow(){
        for(int i=0;i<3;i++){
            if(table[row-1][i]!= CurrentPlayer){
                return false;
          
            }
        }
        return true;
    }
    static boolean checkCol(){
        for(int i=0;i<3;i++){
            if(table[i][col-1] != CurrentPlayer){
                return false;
            }
        }return true;
    }
    static boolean checkDgna() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != CurrentPlayer) 
                return false;
        }
        return true;
    }
    static boolean checkAntiDgna(){
        for(int i=0; i<3; i++){
            if(table[i][(3-1)-i] != CurrentPlayer)
                return false;
        }
        return true;
    }
    
        static boolean isWin(){
        if(checkRow()){
            return true;
        }else if(checkCol()){
            return true;
        }else if(checkDgna()){
            return true;
        }else if(checkAntiDgna()){
            return true;
        }
        return false;
    }
        
    static void printWin(){
        System.out.println(CurrentPlayer + " Win !!");
    }
    static boolean isDraw(){
        return count == 9;
    }
    static void printDraw(){
        System.out.println("It's a draw");
    }
    public static void main(String[] args) {
        printWelcome();
        while(true){
            printTable();
            printTurn();
            inputRowCol();
            if(isWin()){
                printTable();
                printWin();
                break;
            }if(isDraw()){
                printTable();
                printDraw();
                break;
            }
            switchPlayer(); 
        }    
    }
}
